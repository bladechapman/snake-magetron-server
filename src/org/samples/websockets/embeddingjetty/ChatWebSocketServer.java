package org.samples.websockets.embeddingjetty;
 
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
 
public class ChatWebSocketServer {
 
    public static void main(String[] args) {
        try {
            Server server = new Server(8080);
            
            ChatWebSocketHandler chatWebSocketHandler = new ChatWebSocketHandler();
            chatWebSocketHandler.setHandler(new DefaultHandler());
            server.setHandler(chatWebSocketHandler);
            
            server.start();
            server.join();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}