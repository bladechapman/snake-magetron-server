package org.samples.websockets.embeddingjetty;
 
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.http.HttpServletRequest;




//import org.eclipse.jetty.websocket.WebSocketHandler;
import org.eclipse.jetty.websocket.*;

public class ChatWebSocketHandler extends WebSocketHandler {
 
	private final Set<ChatWebSocket> webSockets = new CopyOnWriteArraySet<ChatWebSocket>();
	
    public WebSocket doWebSocketConnect(HttpServletRequest request,
            String protocol) {
    	System.out.println("socket connect initiated");
        return new ChatWebSocket();
    }
    
    private class ChatWebSocket implements WebSocket.OnTextMessage {

    	private Connection connection;
    	
		@Override
		public void onClose(int arg0, String arg1) {
			// TODO Auto-generated method stub
			webSockets.remove(this);
		}

		@Override
		public void onOpen(Connection arg0) {
			// TODO Auto-generated method stub
			this.connection = arg0;
			webSockets.add(this);
		}

		@Override
		public void onMessage(String arg0) {
			// TODO Auto-generated method stub
			try{
				for(ChatWebSocket webSocket : webSockets) {
					webSocket.connection.sendMessage(arg0);
					//System.out.println(arg0);
				}
			} catch(IOException e) {
				this.connection.disconnect();
			}	
		}
    }
}
